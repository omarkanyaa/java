//Autor: Omar Kanyaa Lahasen
//Programa: Loteria
import java.util.Scanner;
public class Loteria {
	public static int pideNum (int inferior, int superior) {
		Scanner teclado=new Scanner(System.in);
		int num;
		do {
			System.out.println("Introduce un número: ");
			num=teclado.nextInt();
		} while (num<inferior || num>superior);
		return num;
	}
	
	public static void main (String [] args) {
		System.out.println("Loteria del Estado");

		int n1, n2, x1, x2;

		n1=pideNum(1,49);
		n2=pideNum(1,49);
		//Generar un numero entero aleatorio ente [1,49] --> [1,50)
		//a*0+b=1 --> b=1
		//a*1+b=50 --> a=49
		x1=(int) (49*Math.random()+1);
		x2=(int) (49*Math.random()+1);
	}
}