#!/bin/bash
echo "Cuanto me falta para suspender"
echo ""
echo "Software desarrollado por: Omar Kanyaa Lahasen"
echo "______________________________________________"

echo "1) Ver si estoy suspenso en Programacion"
echo "2) Ver si estoy suspenso en Lenguaje de Marcas" 
echo "3) Ver si estoy suspenso en Base de Datos"
echo "4) Ver si estoy suspenso en Entorno de Desarrollo"
echo "5) Ver si estoy suspenso en Sistemas Informaticos"
echo "Elija una opcion:"
echo "______________________________________________"
read opcion
case $opcion in
1) java Programacion;;
2) java LenguajeDeMarcas;;
3) java BaseDeDatos;;
4) java EntornoDeDesarrollo;;
5) java SistemasInformaticos;;
*) echo "Opciones de 1 a 5 solamente";;
esac
